file-upload
===========

This project is backend simple media store.

Create database
```bash
php bin/console doctrine:database:create
```

Create tables
```bash
php bin/console doctrine:schema:update --force
```

Load test data into db:
```bash
php bin/console faker:populate
```

For authorization use the following data  
>username: admin  
>password: admin

Do not forget to install [RabbitMQ](https://www.rabbitmq.com/install-debian.html).

For create user in RabbitMQ use
```bash
sudo rabbitmqctl add_user test test
sudo rabbitmqctl set_user_tags test administrator
sudo rabbitmqctl set_permissions -p / test ".*" ".*" ".*"
```

To run the consumer:
```bash
$ php bin/console rabbitmq:consumer send_email
```

For web statistics RabbintMQ in Ubuntu:
```bash
sudo rabbitmq-plugins enable rabbitmq_management
rabbitmq-plugins list
sudo service rabbitmq-server restart
```
Web statistics are available by url http://127.0.0.1:15672