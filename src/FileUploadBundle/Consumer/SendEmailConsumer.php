<?php

//src/FileUploadBundle/Consumer/SendEmailConsumer.php

namespace FileUploadBundle\Consumer;


use Monolog\Logger;
use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;
use Symfony\Bundle\FrameworkBundle\Controller\TemplateController;

class SendEmailConsumer implements ConsumerInterface
{

    private $logger;
    /** @var  $mailer \Swift_Mailer */
    private $mailer;
    /** @var  $twig \Twig_Environment */
    private $twig;

    public function __construct( $logger, $mailer, $twig )
    {
        $this->logger = $logger;
        $this->mailer = $mailer;
        $this->twig = $twig;
        echo "Consumer is listening!" . PHP_EOL;
    }

    public function execute(AMQPMessage $msg)
    {
        $message = unserialize($msg->body);
        $text = \Swift_Message::newInstance()
            ->setSubject('Файл доступен для скачивания')
            ->setFrom($message['from'])
            ->setTo($message['email'])
            ->setBody(
                $this->twig->render(
                    ':emails:file_upload_confirm.html.twig',
                    array(
                        'hash_user' => $message['hash_user'],
                        'hash_file' => $message['hash_file'],
                        'filename' =>  $message['filename'],
                        'file_path' => $message['file_path']
                    )
                ),
                'text/html'
            )
            /*
             * If you also want to include a plaintext version of the message
            ->addPart(
                $this->renderView(
                    'Emails/registration.txt.twig',
                    array('name' => $name)
                ),
                'text/plain'
            )
            */
        ;
        echo $text->getBody();
        echo $this->mailer->send($text);
        //$taskid = $message['taskid'];

        //echo "Processing task number: " .  $taskid . PHP_EOL;
        // Processing

        return true;
    }
}