<?php

namespace FileUploadBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Files
 *
 * @ORM\Table(name="files", uniqueConstraints={@ORM\UniqueConstraint(name="hash_file", columns={"hash_file"})})
 * @ORM\Entity(repositoryClass="FileUploadBundle\Repository\FilesRepository")
 */

class Files
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="hash_file", type="string", length=32, nullable=false)
     */
    private $hashFile;

    /**
     * @var string
     *
     * @ORM\Column(name="filename", type="string", length=255, nullable=false)
     */
    private $filename;

    /**
     * @var string
     *
     * @ORM\Column(name="note", type="string", length=2000, nullable=true)
     * @Assert\Length(
     *      max = 2000
     * )
     */
    private $note;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="tstamp", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     * @ORM\Version
     */
    private $tstamp;

    /**
     * @var string
     *
     * @ORM\Column(name="hash_user", type="string", length=255, nullable=false)
     */
    private $hash_user;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=100, nullable=true)
     * @Assert\Length(
     *      max = 100
     * )
     */
    private $email;

    /**
     * @var
     * @Assert\File(
     *     maxSize = "100M"
     * )
     */
    private $file;

    /**
     * @return string
     */
    public function getHashUser(): string
    {
        return $this->hash_user;
    }

    /**
     * @param string $hash_user
     */
    public function setHashUser(string $hash_user)
    {
        $this->hash_user = $hash_user;
    }


    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }


     /**
     * @return mixed
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param mixed $file
     */
    public function setFile($file)
    {
        $this->file = $file;
    }


    /**
     * Set hashFile
     *
     * @param string $hashFile
     *
     * @return Files
     */
    public function setHashFile($hashFile)
    {
        $this->hashFile = $hashFile;

        return $this;
    }

    /**
     * Get hashFile
     *
     * @return string
     */
    public function getHashFile()
    {
        return $this->hashFile;
    }

    /**
     * Set filename
     *
     * @param string $filename
     *
     * @return Files
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;

        return $this;
    }

    /**
     * Get filename
     *
     * @return string
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * Set note
     *
     * @param string $note
     *
     * @return Files
     */
    public function setNote($note)
    {
        $this->note = $note;

        return $this;
    }

    /**
     * Get note
     *
     * @return string
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * Set tstamp
     *
     * @param \DateTime $tstamp
     *
     * @return Files
     */
    public function setTstamp($tstamp)
    {
        $this->tstamp = $tstamp;

        return $this;
    }

    /**
     * Get tstamp
     *
     * @return \DateTime
     */
    public function getTstamp()
    {
        return $this->tstamp;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    public function getFilePath()
    {
        return '/uploaded/'.$this->hash_user.'/'.$this->hashFile;
    }
}