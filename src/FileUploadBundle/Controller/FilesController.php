<?php

namespace FileUploadBundle\Controller;

use FileUploadBundle\Entity\Files;
use FileUploadBundle\Form\FilesType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * File controller.
 *
 * @Route("files")
 */
class FilesController extends Controller
{
    /**
     * Lists all file entities.
     *
     * @Route("/", name="files_index")
     * @Method("GET")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        return $this->indexPageAction();
    }

    /**
     * Lists all file entities.
     *
     * @Route("/page/{page}", name="files_index_page")
     * @Method("GET")
     * @param int $page
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexPageAction($page = 1)
    {
        $limit = 50;

        $em = $this->getDoctrine()->getManager();

        $files = $em->getRepository('FileUploadBundle:Files')->getAllFiles($page, $limit);

        $maxPages = ceil($files->count() / $limit);

        $deleteForms = array();

        foreach ($files as $file) {
            $deleteForms[$file->getId()] = $this->createDeleteForm($file)->createView();
        }

        return $this->render('files/index.html.twig', array(
            'files' => $files,
            'deleteForms' => $deleteForms,
            'thisPage' => $page,
            'maxPages' => $maxPages,
            'limit' => $limit
        ));
    }


    /**
     * Get file
     *
     * @param $hash_user
     * @param $hash_file
     *
     * @Method("GET")
     * @return BinaryFileResponse
     */
    public function getFileAction($hash_user, $hash_file)
    {
        //var_dump($hash_user);
        //var_dump($hash_file);
        $em = $this->getDoctrine()->getManager();
        $fileDb = $em->getRepository('FileUploadBundle:Files')->findOneBy(array('hash_user' => $hash_user, 'hashFile' => $hash_file));

        if (is_null($fileDb)){
            throw $this->createNotFoundException();
        }

        $file = $this->getParameter('files_directory').'/'.$fileDb->getHashFile();

        // check if file exists
        $fs = new Filesystem();
        if (!$fs->exists($file)) {
            throw $this->createNotFoundException();
        }

        //echo $file;
        $response = new BinaryFileResponse($file);
        $response->trustXSendfileTypeHeader();
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_INLINE,
            $fileDb->getFilename(),
            iconv('UTF-8', 'ASCII//TRANSLIT', $fileDb->getFilename())
        );

        return $response;

    }

    /**
     * Upload new file via ajax
     *
     * @Route("/newAjax", name="files_new_ajax")
     * @Method({"GET","POST","OPTIONS"})
     * @param Request $request
     * @return JsonResponse
     */
    public function newAjaxAction(Request $request)
    {
        //This is optional. Do not do this check if you want to call the same action using a regular request.
        /*if (!$request->isXmlHttpRequest()) {
            return new JsonResponse(array('message' => 'You can access this only using Ajax!'), 400);
        }*/

        $form = $this->createForm(FilesType::class);
        $form->handleRequest($request);

        $error = '';

        if ($form->isSubmitted() && $form->isValid()) {

            $files = $form->getData();
            /** @var $files Files */


            // $file stores the uploaded file
            /** @var $file UploadedFile */
            $file = $files->getFile();

            $files->setFilename($file->getClientOriginalName());

            // Generate a unique name for the file before saving it
            $fileName = md5(uniqid());

            // Move the file to the directory where user avatars are stored
            $file->move(
            // This parameter should be configured
                $this->getParameter('files_directory'),
                $fileName
            );

            // Update the 'avatar' property to store file name
            // instead of its contents
            $files->setHashFile($fileName);
            $files->setHashUser(md5($files->getEmail()));

            $em = $this->getDoctrine()->getManager();
            $em->persist($files);
            $em->flush();

            $msg = array(
                'email' => $files->getEmail(),
                'hash_user' => $files->getHashUser(),
                'hash_file' => $files->getHashFile(),
                'filename' => $files->getFilename(),
                'from' => $this->getParameter('swiftmailer.sender_address'),
                'file_path' => $this->generateUrl('get_file', array('hash_user'=> $files->getHashUser(), 'hash_file' => $files->getHashFile()),UrlGeneratorInterface::ABSOLUTE_URL)
            );
            $this->get('old_sound_rabbit_mq.send_email_producer')->publish(serialize($msg));


            // ... persist the $user variable or any other work and redirect
        } else {
            $error = 'Ошибка загрузки, проверьте данные!';
        }

        $resp = new JsonResponse();
        $resp->headers->set('Access-Control-Allow-Origin', '*');
        $resp->setData($error);
        return $resp;
    }

    /**
     * Finds and displays a file entity.
     *
     * @Route("/{id}", name="files_show")
     * @Method("GET")
     * @param Files $file
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(Files $file)
    {
        $deleteForm = $this->createDeleteForm($file);

        return $this->render('files/show.html.twig', array(
            'file' => $file,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing file entity.
     *
     * @Route("/{id}/edit", name="files_edit")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @param Files $file
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, Files $file)
    {
        $editForm = $this->createForm('FileUploadBundle\Form\FilesUpdateType',$file);
        $editForm->handleRequest($request);


        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('files_edit', array('id' => $file->getId()));
        }

        return $this->render('files/edit.html.twig', array(
            'file' => $file,
            'edit_form' => $editForm->createView(),
        ));
    }

    /**
     * Deletes a file entity.
     *
     * @Route("/{id}", name="files_delete")
     * @Method("DELETE")
     * @param Request $request
     * @param Files $file
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, Files $file)
    {
        $form = $this->createDeleteForm($file);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($file);
            $em->flush();
        }

        return $this->redirectToRoute('files_index');
    }

    /**
     * Creates a form to delete a file entity.
     *
     * @param Files $file The file entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Files $file)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('files_delete', array('id' => $file->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
